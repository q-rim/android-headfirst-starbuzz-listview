package com.example.kyurim.starbuzz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class DrinkCategoryActivity extends Activity {

    private final static String TAG = "---DrinkCatActivity";
    public static final String KEY_DRINK_ID= "drinkId";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink_category);

        Log.i(TAG, "onCreate()");

        // ArrayAdapter - display drinks from Drink class
        ListView listDrinks = (ListView) findViewById(R.id.listView_drinks);
        listDrinks.setAdapter(new ArrayAdapter<Drink>(this, android.R.layout.simple_list_item_1, Drink.drinks));

        // onItemClick
        listDrinks.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i(TAG, "onItemClick() - position:" + position);                                 // do something when clicked

                Intent intent = new Intent(DrinkCategoryActivity.this, DrinkActivity.class);
                intent.putExtra(KEY_DRINK_ID, position);                    // passing data to DrinkActivity
                startActivity(intent);                                      // starting DrinkAcitivity
            }
        });
    }
}
