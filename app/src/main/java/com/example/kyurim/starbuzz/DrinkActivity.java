package com.example.kyurim.starbuzz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class DrinkActivity extends Activity {

    final static String TAG = "---DrinkActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink);

        Log.i(TAG, "onCreate()");

        Intent intent = getIntent();                            // Receiving data from DrinkCategoryActivity
        //int drinkId = intent.getIntExtra(DrinkCategoryActivity.KEY_DRINK_ID, -1);             // Method1
        int drinkId = (Integer) intent.getExtras().get(DrinkCategoryActivity.KEY_DRINK_ID);     // Method2
        Drink drink = Drink.drinks[drinkId];                    // define Drink based on Extra
        Toast.makeText(this, drinkId + ": " + drink.getName() + "\n\n" + drink.getDescription(), Toast.LENGTH_SHORT).show();

        // ImageView - set Image
        ImageView drinkImage = (ImageView) findViewById(R.id.imageView_photo);
        drinkImage.setImageResource(drink.getImageResourceId());

        // TextView - set name
        TextView drinkName = (TextView) findViewById(R.id.textView_name);
        drinkName.setText("Name: " + drink.getName());

        // TextView - set description
        TextView drinkDescription = (TextView) findViewById(R.id.textView_description);
        drinkDescription.setText("Description: " + drink.getDescription());
    }
}
