package com.example.kyurim.starbuzz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.lang.annotation.Target;

public class TopLevelActivity extends Activity {

    final static String TAG = "---TopLevelActivity: ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_level);

        // ListView Implementation
        ListView listView = (ListView) findViewById(R.id.listView_options);                         // Create ListView
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {                     // Create Listener
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {      // onItemClick()  - not onClick()
                Log.i(TAG, "\n***pressed: " + position + "\n***pressed: " + id);                    // position=0 is the first item on ListView
                if (position == 0) {
                    Intent intent = new Intent(TopLevelActivity.this, DrinkCategoryActivity.class);
                    startActivity(intent);
                }
            }
        });
    }
}
