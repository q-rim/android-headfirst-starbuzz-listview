package com.example.kyurim.starbuzz;

/**
 * Created by kyurim on 11/5/17.
 */

public class Drink {

    private String name;                // drink's name
    private String description;         // drink's description
    private int imageResourceId;        // drink's image

    private Drink(String name, String description, int imageResourceId) {
        this.name = name;
        this.description = description;
        this.imageResourceId = imageResourceId;
    }

    public String toString() {
        return this.name;
    }

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public int getImageResourceId() {
        return this.imageResourceId;
    }

    // drinks is an Array of Drinks
    public static final Drink[] drinks = {
        new Drink("Latte", "1 espresso shots with steamed milk", R.drawable.latte),
        new Drink("Cappuccino", "2 espresso shots, hot milk & steamed milk foam", R.drawable.cappuccino),
        new Drink("Filtered", "brewed fresh", R.drawable.filter)
    };

}
